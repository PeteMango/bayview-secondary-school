# Grade 10 Pre-IB Oral Presentation Questions

## Analytical Geometry
### A Triangle has vertices A(-1, 5), B(7, 2), and C(-1, -4)
#### Determine what type of triangle $`\triangle ABC`$ is
$`D_{AB} = \sqrt{(5-2)^2 + [(-1)-7]^2}`$
$`= \sqrt{9 + 64}`$
$`= \sqrt{73}`$


$`D_{BC} = \sqrt{[7-(-1)]^2 + [2-(-4)]^2}`$
$`= \sqrt{64 + 36}`$
$`= \sqrt{100}`$
$`= 10`$


$`D_{AC} = 5 - (-4)`$
$`= 9`$

$`\because AB != BC != AC`$

$`\therefore \triangle ABC`$ $`is`$ $`isoceles`$

$`\cos(A) = \frac{AB^2 + AC^2 - BC^2}{2 \times AB \times AC}`$
$`= \frac{(\sqrt{73})^2 + 9^2 - 10^2}{2 \times \sqrt{73} \times 9}`$
$`= \frac{54}{18 \sqrt{73}}`$
$`A = cos^{-1}(\frac{54}{18 \sqrt{73}})`$ 
$`A \approx 69.44^\circ`$

$`\cos(B) = \frac{AB^2 + BC^2 - AC^2}{2 \times AB \times BC}`$
$`= \frac{(\sqrt{73})^2 + 10^2 - 9^2}{2 \times \sqrt{73} \times 10}`$
$`= \frac{92}{20 \sqrt{73}}`$
$`B = cos^{-1}(\frac{92}{20 \sqrt{73}})`$
$`B \approx 57.43^\circ`$


$`\cos(C) = \frac{AC^2 + BC^2 - AB^2}{2 \times AC \times BC}`$
$`= \frac{9^2 + 10^2 - (\sqrt{73})^2}{2 \times 10 \times 9}`$
$`= \frac{108}{180}`$
$`= \frac{3}{5}`$
$`C = cos^{-1}(\frac{3}{5})`$
$`C \approx 53.13^\circ`$

$`\because \angle A != \angle B != \angle C`$

$`\therefore \triangle ABC`$ $`is`$ $`scalene`$

#### Find the orthocenter of $`\triangle ABC`$
$`m_{AB} = \frac{2-5}{7-(-1)}`$ 
$`= -\frac{3}{8}`$

$`m_{AC} = \frac{5-(-4)}{(-1)-(-1)}`$
$`= undefined`$

$`m_{BC} = \frac{2-(-4)}{7-(-1)}`$
$`= \frac{6}{8}`$
$`= \frac{3}{4}`$

$`\bot m_{AB} = \frac{8}{3}`$

$`\bot m_{BC} = -\frac{4}{3}`$

$`Plug \; in \; Point \; C`$

$`y = \frac{8}{3}x + b`$

$`-4 = \frac{8}{3} \times (-1) + b`$

$`= -\frac{8}{3} + b`$

$`b = -4 + \frac{8}{3}`$
$`= -\frac{4}{3}`$

$`y = \frac{8}{3}x - \frac{4}{3}`$

Plug in Point A

$`y = -\frac{4}{3}x + b`$

$`5 = -\frac{4}{3} \times (-1) + b`$
$`= \frac{4}{3} + b`$

$`b = 5 - \frac{4}{3}`$
$`= \frac{11}{3}`$

$`y = -\frac{4}{3}x + \frac{11}{3}`$

$`Intersection \; of \; \bot AB \; and \; \bot BC`$

$`\frac{8}{3}x - \frac{4}{3} = -\frac{4}{3}x + \frac{11}{3}`$
$`4x = 5`$
$`x = \frac{5}{4}`$

$`y = \frac{8}{3}x - \frac{4}{3}`$
$`y = \frac{8}{3} \times \frac{5}{4} - \frac{4}{3}`$
$`y = 2`$

$`\therefore The \; Orthocenter \; of \triangle ABC \; is \; at \; point \; (\frac{5}{4}, 2)`$

### The endpoints of AB are $`A(\sqrt{72}, -\sqrt{12})`$ and $`B(\sqrt{32}, -\sqrt{48})`$. Find the midpoint.
$`\frac{\sqrt{72}+\sqrt{32}}{2}`$ 
$`=\frac{10\sqrt{2}}{2}`$
$`=5\sqrt{2}`$

$`\frac{-\sqrt{12} + (-\sqrt{48}}{2}`$
$`=\frac{-6\sqrt{3}}{2}`$
$`=-3\sqrt{3}`$

$`The \; midpoint \; of \; AB \; is \; (5\sqrt{2}, -3\sqrt{3})`$

### A design plan for a thin triangular computer component shows the coordinates of the vertices at A(8, 12), B(12, 4) and C(2, 8). Find the coordinate of the center of mass.

$`M_{AB} = (\frac{8+12}{2}, \frac{12+4}{2})`$
$`= (10, 8)`$

$`M_{BC} = (\frac{12+2}{2}, \frac{4+8}{2})`$
$`= (7, 6)`$

$`Plug \; in \; Point \; C`$

$`y = mx + b`$

$`m = \frac{8-8}{10-2}`$
$`= 0`$

$`b = 6 - 0 = 6`$

$`Plug \; in \; Point \; A`$

$`y = max + b`$

$`m = \frac{6-12}{7-8}`$
$`= 6`$

$`b = y - mx`$
$`= 