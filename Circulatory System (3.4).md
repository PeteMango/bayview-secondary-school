# Circulatory System (3.4)

## Circulatory System:
Comprised of `three important parts` <br/>
⚝ Blood <br/>
⚝ Heart <br/>
⚝ Blood vessels

## How does it work?
`Arteries carry oxygenated blood away` from the heart, `exchange substances with surrounding tissues` in the capillaries, `de-oxygenated blood is carried back to the heart through veins`.

## Blood:
A type of `connective tissue` that circulates through our body. Made up of `four components`: <br/>
⚝ **Red blood cells:** Makes up `50%` of the volume and `contain a protein called hemoglobin` that allows oxygen to circulate in our body. It is also what `makes blood look red`. <br/>
⚝ **White blood cells:** Recognizes and `destroys invading bacteria and viruses` and makes up less than `1%` of the volume. <br/>
⚝ **Platelet cells:** Helps in `blood clotting`, comprises of less than `1%`of the volume. <br/>
⚝ **Plasma:** `Protein rich liquid` that carries the blood cells along, makes up `50%` of the volume.

## Heart: 
Made up of `three types of tissues` <br/>
⚝ Cardiac Muscle Tissue (only found in the heart) <br/>
⚝ Nerve Tissue <br/>
⚝ Connective Tissue 

The cardiac muscle tissue in each part of the heart `contracts at the same time`. Your heart rate is highly `depdent on factors such as physical activity, temperature, stress, general health, etc`. 
Your heart is `covered by a layer of epithelial tissue`, which reduces friction and protects the heart when your lungs expand and contract. The inside of the heart is covered by
a `smooth layer of epithelial` to allow blood to flow smoothly, if this layer of epithelial tissue hardens, it might lead to health problems.

## Blood Vessels:
Arteries and Veins tend to be `larger near the heart` and become smaller and smaller the further away you are. <br/>
**Arteries:** Carries blood `away from the heart`, hence is `under greater pressure`, therefore the `walls are thicker`. <br/>
**Veins:** Carries blood `towards the heart`, hence is `under lower pressure`,  therefore the `walls are not as thick`. <br/>
**Capillaries:** `Extremely thin walls` inorder to `allow substances to diffuse between the blood and other tissues`. $`CO_2`$ and other wastes also passes into the blood.

## Check Your Learning
1. Describe the function of the circulatory system <br/>
`The circulatory system transports substances (Eg, proteins and nutrients) around the bodies and eventually carry the waste material back to the kidneys to be excreted.`
2. Name at least four substances that are carried by the circulatory system. <br/>
`The ciculatory system is comprised of blood, which contains red blood cells, white blood cells, platelet cells and plasma.`
3. Explain how the circulatory system interacts with the digestive system. <br/>
`The circulatory system carries chemical signals called endocrine which controls the speed at which we digest our food. The circulatory also helps get all the nutrients absorbed from the digestive system to where it is needed in our body.`
4. How does angiogram differ from a regular x-ray scan? <br/>
`In an angiogram, a flourescant dye is injected into our circulatory to provide coloring to an x-ray scan.`
5. Figure 7 shows cross-sections of three different blood vessels. Name each one and describe how its structure matches its function. <br/>
`The first diagram shows a vein, as the walls are very thin and good for carrying blood back to the heart. The second diagram shows an artery, as it has thick walls that carries blood away from the heart. The third and final diagram depicts a capillary as it is extremely small and allows for substance exchange with other tissues in our body.`
8. How is cardiac muscle different from the smooth muscle that surrounds the digestive tract? <br/>
`The cardiac muscle can only be found in the heart and it is also an involuntary muscle.`
9. Name and briefly describe two diseases or disordersof the circulatory system. <br/> 
`Coronary Artery disease is when part of the artery gets blocked by substances containing fat, cholestrol and calcium. When the artery gets completely blocked, the artery will explod, turning into a heart attack.`