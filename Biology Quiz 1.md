# Biology Quiz 1 Study Sheet <br/> Peter Wang

## Cells
Cells are the smallest functional units of living organisms and all living things are comprised of cells (No Exceptions)

## Cell Theory:
1. All living things are comprised of cells
2. Cells are the basic units of living organisms (Sort of like formula units)
3. All cells come from pre-existing cells (Except the first cell ever)

## Type of Cells 
|Prokaryotic|Eukaryotic|
|:----------|:---------|
|- Single Celled <br/> - May not require oxygen <br/> - DNA is in the nucleoid (Free Flowing) <br/> - Relatively small <br/> - Organelles are not bound by a membrane|- Often multicellular <br/> - Often requires oxygen <br/> - DNA is in the nucleus <br/> - Relatively large <br/> - Organelles are bound by a membrane|

## Organelles
- Memebrane bound structures that lives within the cell
- Performs unique functions for the cell

### Cell Membrane
- It acts as the outer covering of the cell
- Semi-permeable (Able to control what enters and leaves)

### Cytoplasm
-  Jelly-like substance that holds the organelles in place

### Cytoskeleton
- "Skeletons" of the cell
- Able to change the shape of the cell in a flash
- Made of microtubules
- Acts as "train tracks" for vesicles moving around the cell

### Ribosomes
- Made in the nucleus 
- Their purpose is to create proteins using the information from the DNA
- Found free flowing in the cell attached to the Endoplasmic Reticulum (ER)

### Lysosomes
- Contains acids that is used to digest food
- Also able to destroy invaders
- Can recyle raw material into new materials
- When a cell senses that something is wrong, it begins cellular suicide (Open the lysosome and let the acid destroy the cells)

### Vacuoles
- Stores water and nutrients

### Mitochondria
- Performs cellular respirtation (Sunlight + Oxygen --> Carbon Dioxide + Water + ATP (Energy))
- Can also be performed in the cytoplasm, but is most efficiently done in the mitochondria

### Vesicles
- Transport proteins and other materials around the cell
- Made up of the same material as the cell membrane
- When stuff arrives through the cell membrane, a vesicle is created to transport it around and when cells leaves, it becomes part of the cell membrane again
- The endoplasmic reticulum can also create vesicles

### Endoplasmic Reticulum
- Network of tubules 
- Has ribosomes attached to them to synthesize proteins
- Proof reads the DNA made by the ribosomes
- Creates temporary vesicles to transport the proteins to the golgi bodies

### Nucleus
- Houses and protects the DNA and the nucleolus

### Nucleolus
- Creates ribosomes
- The part of the nucleus with high DNA concentration

## Plant Cells
### Cell Wall
- Made out of cellulose (A type of sugar)
- Provides structural support to the cell
- Strong, more rigid version of the cell membrane
- Also provides light protection

### Chloroplasts
- Performs photosynthesis (Sunlight + Carbon Dioxide + Water --> Glucose + Oxygen)
- Plants need the chloroplast as they cannot get energy from food 

### Central Vacuoles
- Giant vacuole in the center of the cell
- Maintains the turgor pressure in the cell (High Concentration of water wanting to leave the cell pushes against the cell walls)
- Keeps the plant cells rigid
- This is not found in animal cells as they lack a cell wall, hence this much water would cause the cell to explod
- When plants lack water, the central vacuole is not filled, hence the turgor pressure decreases and the plant wilts

## Plant VS Animal Cells
### Plant Cells:
- Central Vacuole
- Chloroplast
- Cell Wall

### Animal Cells:
- Centrioles
- Centrosomes

## Cell Division:
- Reproduction
- Growth
- Repair

### Reproduction:
- The only way for cells to evolve
- Both single and multicellular organisms  will divide to reproduce
- **Asexual Reproduction:**
    - Single celled organisms reproduce through asexual means
    - Offsprings are exact genetic copies of the parent
    - Genetic discrepencies results in evolution
- **Sexual Reproduction:**
    - Most multicellular organisms reproduce through sexual means, however some are able to reproduce asexually
    - Sex cells from each parent combines to form 2 daughter cells and each daughter cell will inherit characteristics from both parents
    - Reproducting sexually will cause evolution to occur faster
    - Each daughter cells only contain half of the DNA from a parent and another half from another parent

### Growth:
- The amount of time it takes to transport nutrients and waste materials around the cell limits its size (Transportation of nutrients occur naturally from diffusion and osmosis)
- When the cell becomes too large, it will take too long for nutrients to get to where it is needed
- Inorder to maintain proper function and to grow, cell must divide
    - This allows multicellular organisms to form elaborate organs and systems that can perform specific tasks more efficiently  

### Repair:
- Organisms need to repair their cells inorder to stay alive and healthy
- Replace **damaged cells**:
    - Cuts
    - Bruises
    - Broken bones
- Replace **dead cells**: 
    - Skin shedding
    - Acidified epithelial cells

## Cell Specialization
- Humans start their life as special cells call zygoats
    - The location of the cell in the body and chemical messages from other cells will determine which genes will be activated and what type of cell it will grow into
- **Stem Cells**
    - After cell division, either both cells stay the same or one of the cells can specialize 
    - Applications of stem cells:
        - Test new medicines
        - Produce animal meats
        - Study how cells grow and function


## Stem Cells
- Totipotent
- Pluripotent
- Multipotent "Adult" Tissue 
- Induced pluripotent 

### Totipotent Stem Cells
- Also known as **totally potential** stem cells
- Can become any type of cells, even placenta or a whole new organism (Eg. Cloning)
- These cells can be found in the **morula** (16 cell ball) in the first 3-4 days after the zygoat forms
- **Pros**
    - Unlimited potential
- **Cons**
    - Destroys newly created embryos (ethical concern)

### Pluripotent Stem Cells
- Taken from the **blastocyst** (200-300 cell cluster in an early embryo) 4-7 days after fertilization
- Can be found in discarded in vitro fertilization embryos
- They can becomes any type of cell but cannot become a new embryo
- **Pros**
    - No need for a new embryo, the ones they are taken from were originally going to be discarded anyways
- **Cons**
    - Still destroys and embryos
    - Difficult to find "immunological match" if being used for injury treatment

### Multipotent "Adult" Tissue Stem Cells
- Can be taken from any adult patient's body tissues (Eg. Bone Marrow)
- Limited to a few cell type, depnding on the organ that they came from

### Induced Pluripotent Stem Cells
- Discovered in 2006
- Adult stem cells can be reprogrammed with 4 embryotic genes through a modified virus
- Can be chemically treated to become any adult cell
- **Pros**
    - Does not require a new embryo
    - Cells are specific to the patient and won't casue a reaction from the immune system
- **Cons**
    - Currently still working out the details of controlling specialization

## Uses of Stem Cells
- Study how different cells grow and function
- Test new medicine on specific cells
- Mass produce cultured meat so no more animals will be slaughtered
- Regenerative medicine to replace damaged and dead tissues

## Cell Type:
- **Sex Cells:** Egg and Sperm
- **Somatic Cells:** Everything else

## Cell Cycle & Mitosis
- Different cells have different lifespans
    - Stomach Lining: Up to 5 days
    - Nerve cells: Entire life
- Regardless of the type of cells, each cell goes through the same cycle 

## Cell Cyle:
- Interphase
- Mitosis
- Cytokinesis

### Interphase
The phase where the cell performs its normal functions and its genetic material is being duplicated to prepare for cell division. All life activies are carried out in this phase except for divison. DNA strands are also duplicated so that there are two identical strands of genetic material.
- **G1:** Normal Growth and Function
- **S:** Duplication of DNA (Preparation for cell division)
- **G2:** Duplication of organelles 

### Mitosis
Duplication of the genetic material in the nucleus and division of the cell to produce two identical daugther cells.
- Prophase
- Metaphase
- Anaphase
- Telophase

#### Prophsae
- Chromatins condenses to become chromosomes
- Centrosomes and centrioles began moving towards opposite poles
- Nuclear membrane begans to dissolve 
- Spindle fibers start to grow from the centrosomes and began attaching to the centromeres

#### Metaphase
- Nuclear membrane has completely dissolved
- Chromosomes / centromeres line up along the equator of the cell
- Centrosomes with with centrioles have reached the poles
- Spindle fibers have attached to centromeres

#### Anaphase
- Spindle fibers began to retract pulling at the centromeres until they split into sister chromatids which are pulled towards opposite poles
- Sister chromatids are now called daughter chromosomes

#### Telophase
- Nuclear membrane reforms around each group of daughter chromosomes
- Chromosomes uncoil into chromatin strands
- Spindle fiber break apart and dissapear into the cytoplasm
- **Cytokinesis usually begins here, not always**

#### Cell will remain in Interphase if:
- DNA is damaged
- DNA in the nucleus has not being replicated
- Not enough nutrients to provide for cell growth
- Chemical signals from surrounding cells telling it to not divide

### Vocabulary
#### Chrosomes
Any piece of supercoiled DNA, high packed chromatin which becomes visible during mitosis and is not readable.

#### Chromatin
Non-supercoiled DNA that chills in the nucleus. Made up of DNA and proteins to hold it together.Chromatins are a lower order of DNA organization while chromosomes are a higher level of DNA organization.

#### Sister Chromatids
One of the two identical halves of a x-shaped chromosome

#### Centromere
The middle region of the chromatid DNA that holds the sister chromatids together

### Cytokinesis
- **C:** Division of the cytoplasm and everything else

#### Cytokinesis for Animal Cells:
The cell membrane is pulled inwards by the cytoskeleton and forms the "clevage furrow"

#### Cytokinesis for Plant Cells:
Golgi bodies produces and sends vesciles containing material from the cell wall to the equator of the cell. Forms a cell plate that divides the cell into two sections.

## Cancer
### What is Cancer:

### How does cancer cells develop?
1. The mutation gives the cell *advantage over other cells*, speeding up their replication process, growing the tumor.
2. After the next mutation, an "aggresive" form of the cancer mutats and spreads to differnt parts of the body

### Causes of Cancer
- **Chemicals:**
    - Foods
    - Cigarette
    - Industrial pollution
- **Energy:**
    - X-rays
    - UV-rays (Sun and Tanning Beds)
- **Some viruses:**
    - Hepatitis B --> Liver cancer
    - HPV --> Cervical

1. **Cellular:**
    - During the S phase of the cell cycle, the cell doesn't copy its DNA correctly
2. **Envrioonmental:**
    - Mutations caused by carcinogens such as 
        - Sunlight (10%)
        - Tobacco (30%)
        - Diet (35%)
        - Radiation (1%)
        - Alcohol (3%)
        - Occupational Factors (4%)
        - Viruses (7%) 
        - Other (10%)

### Cancer Detection
Best way is to detect and treat the cancer cells early on. Most of the time however, when a cancer cell becomes detectable, it may contain millions of cells and have been growing for years. 
1. **Screening:** Attempts to detct cancers at very early stages in healthy individuals. 
    - PAP Smear - Sticks a swab into the vagina (Cervical Cancer) --> Women
    - Mammogram - Scan of breast tissue (Breast Cancer) --> Women
    - Colonoscopy - Fiber optics camera up the anus (Colorectal Cancer) --> Men & Women (Goes through your anus)
    - PSA Blood Test (Not a Good Test, Mr. Rozen's Father died even tho he got the test every year) - Measures the amount of Prostate-Specific Antigen (Prostate Cancer) - Men
2. **Detection:** A biospy is the removal of a sample of body tissue for examination.
    - Biospies of the respiratory and digestive system cancer can be taken with an endoscope (Cuts open a section of your tissue)
3. **Diagnosing Imaging Technology:** 
    - Endoscopy: Using a flexible camera with built-in tissue extractor (Goes through your mouth)
    - X-Ray: Using radiation / energy to produce images of tissues (Shows spots that are reflecting more x-ray)
    - Ultrasound: Sound waves to create imaging of soft body tissue (used for pregnant women)
    - CT / CAT Scan: Computer axial tomography creates 3D image using X-rays & computer analysis (Inject barium to reflect)
    - Magnetic Resonance Imaging (MRI) Scan: Using magnetic fields, radio waves and computer analysis to create 3D iamges (Safer than CT scan)

### Cancer Treatement
By cutting the patient open, doctors can physically remove the cancerous tissues before it has an opportunity to spread to other regions of the body (Eg. Metastasis). Very difficult to accomplish without harming healthy cells as our body thinks that cancer cells are our own cells.
- Physically cutting out the cancer cell
- Chemotherpy: Treatement with a series of drugs that specically target rapidly dividing cells. Can also harm healthy cells.
- Radiation Therapy: Focusing a high-energy ray to damage a cell's DNA, keeping cancerous cells from dividing, but it can also harm healthy cells. Healthy cells can recover from radiation better than cancerous cell.
- Biophotonics: Using beams of light to detect and treat cancer. Minimally-invasive, electro-optical probe could immediatly clear a large portion of awomen who could otherwise face tremendous physical treatement.

## Post Lab Questions
1. To study human genes, scientists must first extract DNA from human tissue. Explain whether you would expect the method of DNA extraction to be the same or different for human tissue as compared to Fragaria.
    - No need to crush or grind up the cell wall
    - Requires alcohol (Precipitates DNA), soap (Dissolves tthe cell & nuclear membrane) and salt (Allows proteins to dissolve)
2. Explain whether or not the DNA would be the same in any cell in the human body.
    - All cells should be the same exept cancerous cells
    - Sperm and eggs cells would also be different from other cells
3. State the type of tissue you would choose to use to extract DNA from a living person. Explain your choice. 
    - Hair --> Easy, not painful
    - Skin --> Easy, not painful
    - Saliva (Cheek Cell) --> Easy, not painful
4. Describe one source of error/limitation of this experiment. Explain whether this limitation would change the mass of DNA extracted (either higher or lower), or whether it would decrease our certainty about the mass (greater spread of data over successive trials). You must only choose and discuss one of these two options. Explain what could be done (a change in procedure or materials/equipment used to solve this problem if you were to perform this experiment and measure the mass of DNA produced.
    - Crashing strawberry by hand could miss bits: Use a blender to break down the strawberry
    - Varying mass/volume of strawberries: Define a specific mass of strawberries to use
    - Transferring strawberry from bag to filter: Use water to filter our the remaining strawberries in the bag
    - Mashed up the fruit and not the ceed: Use a blender to crush the seeds

## Unit Review
1. What are the three stages in the cell cycle? <br/>
Interphase, Mitosis and Cytokinesis
2. During which stage of the cell cycle does the replication of DNA take place? <br/>
The DNA is replicated in the interphase
3. List and briefly describe the phases of mitosis. 
- Prophase
- Metaphase
- Anaphase
- Telophase
4. a) Identify whether each of the cells shown is an animal cell or a plant cell. <br/>
i) Plant <br/>
ii) Plant <br/>
iii) Animal <br/>
iv) Animal 
4. b) Which stage of the cell cycle (and, if appropriate, which phase of mitosis) is represented in each diagram?
i) Prophse <br/>
ii) Cytokinesis
iii) Prophase <br/>
iv) Cytokinesis
5. What are some features of cancer cells that make them dangerous? <br/>
Cancer cells are able to reproduce at an astonishing speed and they are hard to treat as they tend to be different every time
6. You can oft en smell food cooking even though you may be several rooms away from the kitchen. What process is responsible for the odour spreading through the house? <br/>
The scent of the food can spread throughout the house through a process of diffusion.
7. Briefl y describe the diff erences between plant and animal cell division. <br/>
The cytokinesis of plant cell is different than the cytokinesis of animal cells as plant cells pinch to create the cleavage furrow while the plant cell builts a wall in the middle of the cell to separate the daughter cells
8. Explain in your own words why mitosis is important to eukaryotes <br/>
Mitosis is important as eukaryotes have a nucleus, hence mitosis will be a vital step in their cell division process.
9. How would the cell cycle of a cancer cell be different from that of a normal cell?  <br/>
During the S phase of the cell cycle, the DNA is not copied correctly.
10. What structures are responsible for the movement of chromosomes during mitosis? <br/>
The centrosome and centriole cretes spindle fiber which pulls apart the chromosomes. The chromosomes automatically moves to the equator of the cell as they prepare for cell division.
11. Explain the diff erence between a chromosome and a chromatid. <br/>
Chromosome is a condensed version of a chromatid and only visible during mitosis.
12. a) Explain why there is a maximum size for cells to be able to function effi ciently. <br/>
Inorder for a cell to function efficiently, the cannot grow too large as it would take too long for water and nutrients to move around the cell and can cause the cell to die.
12. b) Very active cells, such as muscle cells, tend to be smaller than fat storage cells. Explain why this is true. <br/>
Muscle cells are very active and requires a constant supply of nutrients and water and if the cells are too large, they cannot transfer nutrients around quickly.
13. Why are skin cells so susceptible to cancer? <br/>
Skin cells are constantly reproducing and dividing allowing for many sources of error, causing mutation resulting in cancer cells.
14. Identify one major part of a plant where cells undergo frequent mitosis
At the tip of the root, there requires constant supply of nutrients. Hence the cells would divide constantly and will therefore undergo frequent mitosis.
15. List at least three “prevention and screening” steps that you can take to reduce your risk of premature death from cancer. <br/>
Mammogram can be used to detect breast cancer. PAP Smear can be used to detect cervical cancer. Colonoscopy an be used to detect clorectal cancer. PSA blood test can be used to test for prostate cancer.
16. Table 1 shows the number of cells in interphase, each phase of mitosis, and cytokinesis for two samples (Sample A and Sample B). For each sample, the percentage of cells in a stage or phase represents the length of time (as a percentage of the cell cycle) that that stage or phase takes. For example, if 50 % of the cells in a sample were in interphase, you would conclude that interphase takes up 50 % of the time of a complete cell cycle. <br/>
17. You notice that a mole on the back of your friend’s arm seems to be getting bigger. Your friend does not think it is a big deal and is planning to ignore it. What would you advise? <br/>
I would advice my friend to get his mole tested for skin cancer as moles tend to cause mutations in our cells.
18. What does sunlight have in common with environmental chemicals that are believed to cause cancer? <br/>
Sunlight contains radiation which will cause mutation in our cells.
19. Why is it important for women who may be pregnant not to undergo X-rays? <br/>
The radiation from the x-ray can severly damage the unborn baby.

## Organising Vocabulary (Smallest --> Largst):
1. Atoms
2. Molecules
3. Cells
4. Tissues
5. Organelles
6. Organs
7. Organ system
8. Organisms
9. Population
10. Community
11. Ecosystem
12. Biome
13. Biosphere

## Evolution
There are only so many ways to solve the same problem --> Animals ill likely evolve similar solutions in their bodies (organs) to transport, move, use food, etc. <br/>
All animals have a common ancestor which would have those kinds of systems.

## Types of Digestive Systems
- `Bag System:` Found in organisms like jellyfish and coral, this digestive system only have `1 opening`. Food and waste enter and exit at the same place. Enzymes are secreted into this cavity to chemically break down food.
- `Tube System:` Founnd in animals like humans, worms and cows. The digestive system has a opening  at both ends of a long tube `one entrance and one exit`.